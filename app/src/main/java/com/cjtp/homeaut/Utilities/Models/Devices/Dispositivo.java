package com.cjtp.homeaut.Utilities.Models.Devices;

/**
 * Created by Torrancio on 27/7/2016
 */
public class Dispositivo {

    private static int num_dispositivos = 0;

    public enum TIPO_DISPOSITIVO{
        CON_RANGO,
        BINARIO,
        MULTI_ESTADO
    }

    protected String nombre;
    protected String descripcion;
    protected boolean activo;
    protected TIPO_DISPOSITIVO tipoDeDispositivo;
    private int ID;

    public Dispositivo(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        tipoDeDispositivo = TIPO_DISPOSITIVO.BINARIO;
        ID = num_dispositivos;
        num_dispositivos++;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void activar(){
        activo = true;
    }

    public void desactivar(){
        activo = false;
    }

    public int getID() {
        return ID;
    }

    public boolean estaActivo(){
        return activo;
    }
}
