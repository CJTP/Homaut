package com.cjtp.homeaut.Utilities.Models.Devices;

/**
 * Created by Torrancio on 27/7/2016
 */
public class DispostivoConRango extends Dispositivo {

    private int rangoMinimo;
    private int rangoMaximo;
    private int aumento;

    private int valorActual;

    public DispostivoConRango(String nombre, String descripcion, int rangoMinimo, int rangoMaximo, int aumento) {
        super(nombre, descripcion);
        this.rangoMinimo = rangoMinimo;
        this.rangoMaximo = rangoMaximo;
        this.aumento = aumento;
        this.valorActual = 0;
    }

    public void aumentar(){
        valorActual += aumento;
        validar();
    }

    public void disminuir(){
        valorActual -= aumento;
        validar();
    }

    private void validar(){
        if(valorActual > rangoMaximo)
            valorActual = rangoMaximo;
        else if(valorActual < rangoMinimo)
            valorActual = rangoMinimo;
    }
}
