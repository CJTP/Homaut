package com.cjtp.homeaut.Utilities.Models.Devices;

/**
 * Created by Torrancio on 27/7/2016
 */
public class DispositivoBinario extends Dispositivo {

    private boolean encendido;

    public DispositivoBinario(String nombre, String descripcion) {
        super(nombre, descripcion);
        encendido = false;
    }

    public void encender(){
        if(estaActivo()){
            encendido = true;
        }
    }

    public void apagar(){
        if(estaActivo()){
            encendido = false;
        }
    }

}
