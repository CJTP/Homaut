package com.cjtp.homeaut.Utilities.Models.Places;

import java.util.ArrayList;

/**
 * Created by Torrancio on 27/7/2016
 */
public class Recinto {

    private static int numero_recintos = 0;

    private String nombre;
    private String descripcion;
    private ArrayList<Integer> dispositivosAsociados;
    private int ID;

    public Recinto(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        dispositivosAsociados = new ArrayList<>();
        ID = numero_recintos;
        numero_recintos++;
    }

    public void asociarDispositivo(int id){
        dispositivosAsociados.add(id);
    }

    public void quitarDispositivo(int id){
        dispositivosAsociados.remove(id);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getID() {
        return ID;
    }

    public int getNumeroDispositivos(){
        return this.dispositivosAsociados.size();
    }
}
