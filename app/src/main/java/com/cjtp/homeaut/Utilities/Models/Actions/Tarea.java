package com.cjtp.homeaut.Utilities.Models.Actions;

import java.util.Date;

/**
 * Created by Torrancio on 27/7/2016
 */
public class Tarea {

    private static int numero_tareas = 0;

    private int ID;
    private String nombre;
    private String descripcion;
    private Date horaDeEjecucion;
    private boolean periodica;

    public Tarea() {
        ID = numero_tareas;
        numero_tareas++;
    }

    public int getID() {
        return ID;
    }

}
