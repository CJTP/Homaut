package com.cjtp.homeaut.Utilities.Models;

import com.cjtp.homeaut.Utilities.Models.Actions.Tarea;
import com.cjtp.homeaut.Utilities.Models.Devices.Dispositivo;
import com.cjtp.homeaut.Utilities.Models.Places.Recinto;

import java.util.ArrayList;

/**
 * Created by Torrancio on 27/7/2016
 */
public class Homaut {

    private ArrayList<Recinto> recintos;
    private ArrayList<Dispositivo> dispositivos;
    private ArrayList<Tarea> tareas;

    public Homaut() {
        recintos = new ArrayList<>();
        dispositivos = new ArrayList<>();
        tareas = new ArrayList<>();
    }

    public void añadirDispotivio(Dispositivo d){
        dispositivos.add(d);
    }

    public void añadirRecinto(Recinto r){
        recintos.add(r);
    }

    public void añadirTarea(Tarea t){
        tareas.add(t);
    }

    public void quitarDispositivo(Dispositivo d){
        dispositivos.remove(d);
    }

    public void quitarRecinto(Recinto r){
        recintos.remove(r);
    }

    public void quitarTarea(Tarea t){
        tareas.remove(t);
    }

    public Dispositivo buscarDispositivoPorID(int id){
        for(Dispositivo d:dispositivos)
            if(d.getID() == id)
                return d;
        return null;
    }

    public Recinto buscarRecintoPorID(int id){
        for(Recinto r:recintos)
            if(r.getID() == id)
                return r;
        return null;
    }

    public Tarea buscarTareaPorID(int id){
        for(Tarea t:tareas)
            if(t.getID() == id)
                return t;
        return null;
    }

    public ArrayList<Dispositivo> getDispositivos() {
        return dispositivos;
    }

    public ArrayList<Recinto> getRecintos() {
        return recintos;
    }

    public ArrayList<Tarea> getTareas() {
        return tareas;
    }
}
