package com.cjtp.homeaut.Utilities.Models.Devices;

import java.util.ArrayList;

/**
 * Created by Torrancio on 27/7/2016
 */
public class DispositivoMultiEstado extends Dispositivo{

    private ArrayList<Estado> estados;
    private int estadoActual;

    public DispositivoMultiEstado(String nombre, String descripcion) {
        super(nombre, descripcion);

        estados = new ArrayList<>();
        estados.add(new Estado("Desactivado", "Añada estados al dispositivo"));
        estadoActual = 0;
    }

    public void añadirEstado(Estado e){

    }

    public void quitarEstado(Estado e){

    }

}
