package com.cjtp.homeaut.Interfaces;

import com.cjtp.homeaut.Utilities.Models.Actions.Tarea;
import com.cjtp.homeaut.Utilities.Models.Devices.Dispositivo;
import com.cjtp.homeaut.Utilities.Models.Places.Recinto;

import java.util.List;

/**
 * Created by Torrancio on 27/7/2016
 */
public interface OnMainActivityFragmentsListener {

    //Definir estos métodos en los fragments necesarios
    //Implementar (y sobreescribir) estos métodos en el MainActivity.java

    void añadirRecinto(Recinto r);
    void añadirDispositivo(Dispositivo d);
    void añadirTarea(Tarea t);

    void quitarRecinto(Recinto r);
    void quitarDispositivo(Dispositivo d);
    void quitarTarea(Tarea t);

    Dispositivo buscarDispositivoPorID(int id);
    Recinto buscarRecintoPorID(int id);


    void actualizarListaRecintos();
    void actualizarListaDispositivos();
    void actualizarListaTareas();

    List<Recinto> getRecintos();
    List<Dispositivo> getDispositivos();
    List<Tarea> getTareas();


}
