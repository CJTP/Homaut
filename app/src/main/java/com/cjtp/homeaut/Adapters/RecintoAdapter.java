package com.cjtp.homeaut.Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cjtp.homeaut.R;
import com.cjtp.homeaut.Utilities.Models.Places.Recinto;

import java.util.List;

/**
 * Created by Torrancio on 27/7/2016
 */
public class RecintoAdapter extends RecyclerView.Adapter<RecintoAdapter.RecintoViewHolder> {

    //Lista interna para elementos
    List<Recinto> recintos;


    //Constructuctor del adaptador, recibe una lista de los elementos a cargar
    public RecintoAdapter(List<Recinto> recintos){
        this.recintos = recintos;
    }

    //Actualiza la lista de elementos
    public void updateList(List<Recinto> recintoList) {
        this.recintos = recintoList;
    }


    //Clase contenedora de elemento RecycleView
    public static class RecintoViewHolder extends RecyclerView.ViewHolder{

        //Campos correspondiente a los elementos del template*.xml correspondiente
        CardView recintoCardView;
        TextView recintoName;
        TextView recintoCantidadDispositivos;


        //Constructor del "contenedor", recive el template lógico
        public RecintoViewHolder(View itemView) {
            super(itemView);

            //Se instancian los elementos lógicos correspondientes al template
            recintoCardView = (CardView) itemView.findViewById(R.id.CV_Lugar);
            recintoName = (TextView) itemView.findViewById(R.id.TV_nombre_lugar);
            recintoCantidadDispositivos = (TextView) itemView.findViewById(R.id.TV_cantidad_dispositivos);


        }
    }


    //Métodos sobreescritos de la clase padre (que deben llamarse al crear el objeto cuántico)
    @Override
    //A medida que sea crean nuevos contenedores "inflar" los temlate (ViewGroup)
    public RecintoAdapter.RecintoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.plantilla_lugar,parent,false);

        RecintoViewHolder recintoViewHolder = new RecintoViewHolder(view);

        return recintoViewHolder;
    }

    @Override
    //Al darle posición al elemento que entra(se hace visible) se "settea" su contenido.
    public void onBindViewHolder( RecintoAdapter.RecintoViewHolder  holder, int position) {

        holder.recintoName.setText(recintos.get(position).getNombre());
        holder.recintoCantidadDispositivos.setText(String.valueOf(recintos.get(position).getNumeroDispositivos() ));

    }

    @Override
    //Cuenta los elementos de la lista
    public int getItemCount() {
        return recintos.size();
    }

    @Override
    //Un **** de la *****
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
