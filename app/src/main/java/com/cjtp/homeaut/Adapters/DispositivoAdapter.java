package com.cjtp.homeaut.Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.cjtp.homeaut.R;
import com.cjtp.homeaut.Utilities.Models.Devices.Dispositivo;
import com.cjtp.homeaut.Utilities.Models.Places.Recinto;

import java.util.List;

/**
 * Created by Torrancio on 28/7/2016
 */
public class DispositivoAdapter extends RecyclerView.Adapter<DispositivoAdapter.DispositivoViewHolder>{

    //Lista interna para elementos
    List<Dispositivo> dispositivos;


    //Constructuctor del adaptador, recibe una lista de los elementos a cargar
    public DispositivoAdapter(List<Dispositivo> dispositivos){
        this.dispositivos = dispositivos;
    }

    //Actualiza la lista de elementos
    public void updateList(List<Dispositivo> dispositivoList) {
        this.dispositivos = dispositivoList;
    }


    //Clase contenedora de elemento RecycleView
    public static class DispositivoViewHolder extends RecyclerView.ViewHolder{

        //Campos correspondiente a los elementos del template*.xml correspondiente
        CardView dispositivoCardView;
        TextView dispositivoName;
        TextView dispositivoDescripcion;
        Switch aSwitch_estado;


        //Constructor del "contenedor", recive el template lógico
        public DispositivoViewHolder(View itemView) {
            super(itemView);

            //Se instancian los elementos lógicos correspondientes al template

            dispositivoCardView = (CardView) itemView.findViewById(R.id.CV_Dispositivo);
            dispositivoName = (TextView) itemView.findViewById(R.id.TV_nombre_dispositivo);
            dispositivoDescripcion = (TextView) itemView.findViewById(R.id.TV_descripcion_dispositivo);
            aSwitch_estado = (Switch) itemView.findViewById(R.id.switch_estado);

        }
    }


    //Métodos sobreescritos de la clase padre (que deben llamarse al crear el objeto cuántico)
    @Override
    //A medida que sea crean nuevos contenedores "inflar" los temlate (ViewGroup)
    public DispositivoAdapter.DispositivoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.plantilla_dispositivo ,parent,false);

        DispositivoViewHolder dispositivoViewHolder = new DispositivoViewHolder(view);

        return dispositivoViewHolder;
    }

    @Override
    //Al darle posición al elemento que entra(se hace visible) se "settea" su contenido.
    public void onBindViewHolder( DispositivoAdapter.DispositivoViewHolder  holder, int position) {

        holder.dispositivoName.setText(dispositivos.get(position).getNombre());
        holder.dispositivoDescripcion.setText(String.valueOf(dispositivos.get(position).getDescripcion() ));
        holder.aSwitch_estado.setEnabled(dispositivos.get(position).estaActivo());

    }

    @Override
    //Cuenta los elementos de la lista
    public int getItemCount() {
        return dispositivos.size();
    }

    @Override
    //Un **** de la *****
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
