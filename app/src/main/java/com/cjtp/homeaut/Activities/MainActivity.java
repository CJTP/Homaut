package com.cjtp.homeaut.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.SharedElementCallback;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.FrameLayout;

import com.cjtp.homeaut.Fragments.AgregarLugarDialogFragment;
import com.cjtp.homeaut.Fragments.DispositivosFragment;
import com.cjtp.homeaut.Fragments.LugaresFragment;
import com.cjtp.homeaut.Interfaces.OnMainActivityFragmentsListener;
import com.cjtp.homeaut.R;
import com.cjtp.homeaut.Utilities.Models.Actions.Tarea;
import com.cjtp.homeaut.Utilities.Models.Devices.Dispositivo;
import com.cjtp.homeaut.Utilities.Models.Homaut;
import com.cjtp.homeaut.Utilities.Models.Places.Recinto;

import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMainActivityFragmentsListener {

    Homaut homaut; //Esto es la casa


    FrameLayout frameLayout_Lugares;
    FrameLayout frameLayout_Dispositivos;

    FragmentManager fragmentManager;

    //FrameLayout.LayoutParams layoutParams_Lugares;
    //FrameLayout.LayoutParams layoutParams_Dispositivos;

//    int screenDensity;

    String TAG = "UNEG "+ this.getClass().getSimpleName();
    String tag = "_______________";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG,tag+"onCreate()"+tag);



        homaut = new Homaut(); //Instancio el sistema y/o la casa.


        //DisplayMetrics metrics = new DisplayMetrics();
        //getWindowManager().getDefaultDisplay().getMetrics(metrics);
        //screenDensity = metrics.heightPixels;


        //Instantiates some xml elements.
        frameLayout_Lugares = (FrameLayout) findViewById(R.id.fl_frament_container_lugares);
        frameLayout_Dispositivos = (FrameLayout) findViewById(R.id.fl_frament_container_dispositivos);

        //Log.d(TAG,"screenDensity: "+Integer.valueOf(this.screenDensity));
        Log.d(TAG,"screenDensity: "+this.screenDensity);

        //VEAMOS
        fragmentManager = this.getSupportFragmentManager();

        addFragment(1);
        addFragment(2);


    }



    public void addFragment(int valor){
        Log.d(TAG,tag+"addFragment("+valor+")"+tag);
        //Creates a fragment "operation"
        FragmentTransaction fragmentTransaction;

        //Starts the fragment operation
        fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment;// = null;
        switch (valor){
            case 1:
                fragment = LugaresFragment.newInstance();
                fragmentTransaction.replace(R.id.fl_frament_container_lugares,fragment,"fragment_lugares");

                break;

            case 2:

                fragment = DispositivosFragment.newInstance();
                fragmentTransaction.replace(R.id.fl_frament_container_dispositivos,fragment);
                break;
            default:
                break;


        }

        // TODO: 3/7/2016 Convertir en un método genérico que funcione con cualquier tipo de frament.
        //Instantiates a determined fragment entity
        //MainFragment mainFragment = MainFragment.newInstance();


        //fragmentTransaction.addToBackStack("pipe"); //para ir a atrás

        //Proceeds to active an action "replace" throw the "operation"
        //fragmentTransaction.replace(R.id.fl_frament_container_main,fragment);
        //fragmentTransaction.replace(R.id.rl_test,mainFragment);

        //Ends the operation...
        fragmentTransaction.commit();
    }

    @Override
    public void añadirRecinto(Recinto r) {

        homaut.añadirRecinto(r);
    }

    @Override
    public void añadirDispositivo(Dispositivo d) {
        homaut.añadirDispotivio(d);
    }

    @Override
    public void añadirTarea(Tarea t) {
        homaut.añadirTarea(t);
    }

    @Override
    public void quitarRecinto(Recinto r) {

    }

    @Override
    public void quitarDispositivo(Dispositivo d) {

    }

    @Override
    public void quitarTarea(Tarea t) {

    }

    @Override
    public Dispositivo buscarDispositivoPorID(int id) {
        return null;
    }

    @Override
    public Recinto buscarRecintoPorID(int id) {
        return null;
    }

    @Override
    public void actualizarListaRecintos() {

        fragmentManager = getSupportFragmentManager();
        LugaresFragment lugaresFragment = (LugaresFragment) fragmentManager.findFragmentByTag("fragment_lugares");
        lugaresFragment.actualizarListaElementos();

    }

    @Override
    public void actualizarListaDispositivos() {

        fragmentManager = getSupportFragmentManager();
        DispositivosFragment dispositivosFragment = (DispositivosFragment) fragmentManager.findFragmentByTag("fragment_dispositivos");
        dispositivosFragment.actualizarListaElementos();

    }

    @Override
    public void actualizarListaTareas() {

    }

    @Override
    public List<Recinto> getRecintos() {

        return homaut.getRecintos();
    }

    @Override
    public List<Dispositivo> getDispositivos() {
        return  homaut.getDispositivos();
    }

    @Override
    public List<Tarea> getTareas() {
        return  homaut.getTareas();
    }


}
