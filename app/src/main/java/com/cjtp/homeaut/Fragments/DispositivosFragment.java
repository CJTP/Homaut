package com.cjtp.homeaut.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.cjtp.homeaut.Adapters.DispositivoAdapter;
import com.cjtp.homeaut.Interfaces.OnMainActivityFragmentsListener;
import com.cjtp.homeaut.R;
import com.cjtp.homeaut.Utilities.Models.Devices.Dispositivo;
import com.cjtp.homeaut.Utilities.Utils.RecyclerItemClickListener;
import java.util.List;


public class DispositivosFragment extends Fragment implements RecyclerItemClickListener.OnItemClickListener  {

    //Lista interna para elementos
    List<Dispositivo> dispositivoList;

    RecyclerView recyclerView;
    //LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;
    DispositivoAdapter dispositivoAdapter;

    private OnMainActivityFragmentsListener fragmentsListener;

    String TAG = "UNEG "+ this.getClass().getSimpleName();
    String tag = "_______________";

    public DispositivosFragment() {
        // Required empty public constructor
    }


    public static DispositivosFragment newInstance(/*String param1, String param2*/) {
        DispositivosFragment fragment = new DispositivosFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG,tag+"onCreateView()"+tag);

        //Instantiates this f* fragment View (The View which belongs to this Fragment)
        View thisFragmentView =
                /**-------- Inflate the layout for this fragment---------------------------------------------*/
                inflater.inflate(R.layout.fragment_dispositivos , container, false);

        //Instantiates some xml elements.
        /**------------------------------------(Inside this f* View called "thisFragmentView")-------*/
        ImageView addingDeviceButtton = (ImageView) thisFragmentView.findViewById(R.id.C_B_add_dispositivo);

        //Implements locally the interface "View.OnClickListener" and its @Overridable method "onClick"--
        View.OnClickListener thisFragmentViewOnClickListener =
                new View.OnClickListener() {

                    @Override/** This method manages all View's Click from "thisFragmentView"*/
                    public void onClick(View v) {
                        switch (v.getId()){
                            case R.id.C_B_add_dispositivo:
                                //Aquí llamo al dialog fragment para agregar dispositivos

                                Log.d(TAG,"Click en agregar dispositivo");
                                showAgregarDispositivosFragment();
                                break;
                            default:
                                break;
                        }

                    }
                };

        //Sets the "on click listeners" to the corresponding listener, which is the locally one created.
        addingDeviceButtton.setOnClickListener(thisFragmentViewOnClickListener);


        recyclerView = (RecyclerView) thisFragmentView.findViewById(R.id.RV_dispositivos);
        recyclerView.setHasFixedSize(true);
        //linearLayoutManager = new LinearLayoutManager(this.getContext());
        gridLayoutManager = new GridLayoutManager(this.getContext(),3);
        //recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setLayoutManager(gridLayoutManager);


        dispositivoList = fragmentsListener.getDispositivos();
        dispositivoAdapter = new DispositivoAdapter(dispositivoList);

        //Shows and creates all elements...
        recyclerView.setAdapter(dispositivoAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),this));


        return thisFragmentView;


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        /*
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
        */
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG,tag+"onAttach()"+tag);

        //Indica al listener que se está escuchando en el contexto de la actividad MainActivity.
        fragmentsListener = (OnMainActivityFragmentsListener) context;


    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
        Log.d(TAG,tag+"onAttach()"+tag);
        //mListener = (OnFragmentInteractionListener) context;
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void showAgregarDispositivosFragment(){
        FragmentManager fm = getFragmentManager();
        AgregarDispositivoDialogFragment agregarDispositivoDialogFragment = new AgregarDispositivoDialogFragment();
        agregarDispositivoDialogFragment.show(fm,"soy_otro_tag");
    }


    public void actualizarListaElementos() {
        dispositivoAdapter.updateList(dispositivoList);
        dispositivoAdapter.notifyDataSetChanged();
    }
}
