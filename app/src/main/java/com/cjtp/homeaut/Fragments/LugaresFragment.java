package com.cjtp.homeaut.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cjtp.homeaut.Adapters.RecintoAdapter;
import com.cjtp.homeaut.Interfaces.OnDialogFragmentsListener;
import com.cjtp.homeaut.Interfaces.OnMainActivityFragmentsListener;
import com.cjtp.homeaut.R;
import com.cjtp.homeaut.Utilities.Models.Homaut;
import com.cjtp.homeaut.Utilities.Models.Places.Recinto;
import com.cjtp.homeaut.Utilities.Utils.RecyclerItemClickListener;

import java.util.List;


public class LugaresFragment extends Fragment implements RecyclerItemClickListener.OnItemClickListener {


    //Lista interna para elementos
    List<Recinto> recintoList;

    RecyclerView recyclerView;
    //LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;
    RecintoAdapter recintoAdapter;

    private OnMainActivityFragmentsListener fragmentsListener;

    String TAG = "UNEG "+ this.getClass().getSimpleName();
    String tag = "_______________";

    public LugaresFragment() {
        // Required empty public constructor
    }

    public static LugaresFragment newInstance(){

        LugaresFragment fragment = new LugaresFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,tag+"onCreate()"+tag);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        Log.d(TAG,tag+"onCreateView()"+tag);

        //Instantiates this f* fragment View (The View which belongs to this Fragment)
        View thisFragmentView =
                /**-------- Inflate the layout for this fragment---------------------------------------------*/
                inflater.inflate(R.layout.fragment_lugares, container, false);

        //Instantiates some xml elements.
        /**------------------------------------(Inside this f* View called "thisFragmentView")-------*/
        ImageView addingPlaceButtton = (ImageView) thisFragmentView.findViewById(R.id.C_B_add_lugar);

        //Implements locally the interface "View.OnClickListener" and its @Overridable method "onClick"--
        View.OnClickListener thisFragmentViewOnClickListener =
                new View.OnClickListener() {

                    @Override/** This method manages all View's Click from "thisFragmentView"*/
                    public void onClick(View v) {
                        switch (v.getId()){
                            case R.id.C_B_add_lugar:
                                //Aquí llamo al dialog fragment para agregar recintos
                                //fragmentsListener.añadirRecinto();
                                Log.d(TAG,"Click en agregar recinto");
                                showAgregarLugaresFragment();
                                break;
                            default:
                                break;
                        }

                    }
                };

        //Sets the "on click listeners" to the corresponding listener, which is the locally one created.
        addingPlaceButtton.setOnClickListener(thisFragmentViewOnClickListener);


        recyclerView = (RecyclerView) thisFragmentView.findViewById(R.id.RV_Lugares);
        recyclerView.setHasFixedSize(true);
        //linearLayoutManager = new LinearLayoutManager(this.getContext());
        gridLayoutManager = new GridLayoutManager(this.getContext(),3);
        //recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setLayoutManager(gridLayoutManager);


        recintoList = fragmentsListener.getRecintos();
        recintoAdapter = new RecintoAdapter(recintoList);

        //Shows and creates all elements...
        recyclerView.setAdapter(recintoAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),this));


        return thisFragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        /*
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
        */
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG,tag+"onAttach()"+tag);

        //Indica al listener que se está escuchando en el contexto de la actividad MainActivity.
        fragmentsListener = (OnMainActivityFragmentsListener) context;


    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
        fragmentsListener = null;
    }

    @Override
    public void onItemClick(View view, int position) {
        Log.d(TAG,tag+"onItemClick("+view.getClass().getSimpleName()+","+position+")"+tag);
        //Quantic listener
        //Click en los elementos en el recycle

    }


    public void showAgregarLugaresFragment(){
        FragmentManager fm = getFragmentManager();
        AgregarLugarDialogFragment agregarLugarDialogFragment = new AgregarLugarDialogFragment();
        agregarLugarDialogFragment.show(fm,"soy_un_tag");
    }

    public void showAgregarDispositivosFragment(){
        FragmentManager fm = getFragmentManager();
        AgregarDispositivoDialogFragment agregarDispositivosDialogFragment = new AgregarDispositivoDialogFragment();
        agregarDispositivosDialogFragment.show(fm,"soy_un_tag");
    }



    public void actualizarListaElementos() {
        recintoAdapter.updateList(recintoList);
        recintoAdapter.notifyDataSetChanged();
    }


}


