package com.cjtp.homeaut.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cjtp.homeaut.Interfaces.OnDialogFragmentsListener;
import com.cjtp.homeaut.Interfaces.OnMainActivityFragmentsListener;
import com.cjtp.homeaut.R;
import com.cjtp.homeaut.Utilities.Models.Places.Recinto;

/**
 * Created by Torrancio on 28/7/2016
 */
public class AgregarLugarDialogFragment extends DialogFragment  {

    EditText editText_lugar_nombre;
    EditText editText_lugar_descripcion;
    Button button_lugar_agregar;
    OnMainActivityFragmentsListener setOnClickListener;
    OnDialogFragmentsListener dialogFragmentsListener;


    String TAG = "UNEG "+ this.getClass().getSimpleName();
    String tag = "_______________";

    public AgregarLugarDialogFragment(){};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.d(TAG,tag+"onCreateView()"+tag);

        //Instantiates this f* fragment View (The View which belongs to this Fragment)
        View thisFragmentView =
                /**-------- Inflate the layout for this fragment---------------------------------------------*/
                inflater.inflate(R.layout.dialog_fragment_add_lugar, container, false);

        //Instantiates some xml elements.
        /**------------------------------------(Inside this f* View called "thisFragmentView")-------*/
        //ImageView addingPlaceButtton = (ImageView) thisFragmentView.findViewById(R.id.C_B_add_lugar);
        editText_lugar_nombre = (EditText) thisFragmentView.findViewById(R.id.ET_Lugar_Nombre);
        editText_lugar_descripcion = (EditText) thisFragmentView.findViewById(R.id.ET_lugar_descripcion);
        button_lugar_agregar = (Button) thisFragmentView.findViewById(R.id.B_lugar_guardar);

        //Sets this dialog fragment title.
        getDialog().setTitle("Agregando Estancia/Lugar");

        //Implements locally the interface "View.OnClickListener" and its @Overridable method "onClick"--
        View.OnClickListener thisFragmentViewOnClickListener =
                new View.OnClickListener() {

                    @Override/** This method manages all View's Click from "thisFragmentView"*/
                    public void onClick(View v) {
                        switch (v.getId()){
                            case R.id.B_lugar_guardar:

                                if(     !editText_lugar_nombre.getText().toString().isEmpty()
                                        &&
                                        !editText_lugar_descripcion.getText().toString().isEmpty()){


                                    //Gets requiered information
                                    String lugar_nombre =  editText_lugar_nombre.getText().toString();
                                    String lugar_descripcion = editText_lugar_descripcion.getText().toString();

                                    //Creates and storages the element...
                                    setOnClickListener.añadirRecinto(new Recinto(lugar_nombre,lugar_descripcion));


                                    //Sale del dialog fragment
                                    dismiss();

                                    //Muestra una alerta

                                    Toast alerta =
                                            Toast.makeText(getContext(),
                                                    "Agregado recinto: ''"+lugar_nombre+"''", Toast.LENGTH_SHORT);

                                    alerta.show();

                                    //dialogFragmentsListener.actualizarListaElementos();
                                    setOnClickListener.actualizarListaRecintos();


                                }else{
                                    Log.d("CJTP","Campos incompletos.");

                                    Toast alerta =
                                            Toast.makeText(getContext(),
                                                    "Introduzca información solicitada", Toast.LENGTH_SHORT);

                                    alerta.show();
                                }


                                Log.d(TAG,"Click en agregar/guardar");

                                break;
                            default:
                                break;
                        }

                    }
                };

        //Sets the "on click listeners" to the corresponding listener, which is the locally one created.
        button_lugar_agregar.setOnClickListener(thisFragmentViewOnClickListener);


        return thisFragmentView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Log.d(TAG,tag+"onAttach()"+tag);

        //Indica al/los listener(s) que se está(n) escuchando en el contexto de la actividad MainActivity y (Otra actividad) respectivamente.
        setOnClickListener = (OnMainActivityFragmentsListener) activity; //acciones al activity (esto está bien)

    }
}
