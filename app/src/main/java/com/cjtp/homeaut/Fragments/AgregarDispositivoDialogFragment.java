package com.cjtp.homeaut.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.cjtp.homeaut.Interfaces.OnDialogFragmentsListener;
import com.cjtp.homeaut.Interfaces.OnMainActivityFragmentsListener;
import com.cjtp.homeaut.R;
import com.cjtp.homeaut.Utilities.Models.Devices.Dispositivo;
import com.cjtp.homeaut.Utilities.Models.Places.Recinto;

/**
 * Created by Torrancio on 28/7/2016
 */
public class AgregarDispositivoDialogFragment extends DialogFragment {

    EditText editText_dispositivo_nombre;
    EditText editText_dispositivo_descripcion;
    Switch aSwitch_dispositivo_estado;

    Boolean aBoolean_switch_aux;

    Button button_dispositivo_agregar;
    OnMainActivityFragmentsListener setOnClickListener;
    //OnDialogFragmentsListener dialogFragmentsListener;


    String TAG = "UNEG "+ this.getClass().getSimpleName();
    String tag = "_______________";

    public AgregarDispositivoDialogFragment(){};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        aBoolean_switch_aux = false;
        Log.d(TAG,tag+"onCreateView()"+tag);

        //Instantiates this f* fragment View (The View which belongs to this Fragment)
        View thisFragmentView =
                /**-------- Inflate the layout for this fragment---------------------------------------------*/
                inflater.inflate(R.layout.dialog_fragment_add_dispositivo, container, false);

        //Instantiates some xml elements.
        /**------------------------------------(Inside this f* View called "thisFragmentView")-------*/
        //ImageView addingPlaceButtton = (ImageView) thisFragmentView.findViewById(R.id.C_B_add_lugar);
        editText_dispositivo_nombre = (EditText) thisFragmentView.findViewById(R.id.ET_Dispositivo_nombre);
        editText_dispositivo_descripcion = (EditText) thisFragmentView.findViewById(R.id.ET_dispositivo_descripcion);
        aSwitch_dispositivo_estado = (Switch) thisFragmentView.findViewById(R.id.switch_estado_base);


        button_dispositivo_agregar= (Button) thisFragmentView.findViewById(R.id.B_dispositivo_guardar);

        //Sets this dialog fragment title.
        getDialog().setTitle("Agregando Dispositivo");

        //Implements locally the interface "View.OnClickListener" and its @Overridable method "onClick"--
        View.OnClickListener thisFragmentViewOnClickListener =
                new View.OnClickListener() {

                    @Override/** This method manages all View's Click from "thisFragmentView"*/
                    public void onClick(View v) {
                        switch (v.getId()){
                            case R.id.B_dispositivo_guardar:

                                if(     !editText_dispositivo_nombre.getText().toString().isEmpty()
                                        &&
                                        !editText_dispositivo_descripcion.getText().toString().isEmpty()){


                                    //Gets requiered information
                                    String dispositivo_nombre =  editText_dispositivo_nombre.getText().toString();
                                    String dispositivo_descripcion = editText_dispositivo_descripcion.getText().toString();

                                    //Creates and storages the element...

                                    Dispositivo dispositivo = new Dispositivo(dispositivo_nombre,dispositivo_descripcion);

                                    if (aBoolean_switch_aux){
                                        dispositivo.activar();
                                    }else {
                                        dispositivo.desactivar();
                                    }


                                    setOnClickListener.añadirDispositivo(new Dispositivo(dispositivo_nombre,dispositivo_descripcion));


                                    //Sale del dialog fragment
                                    dismiss();

                                    //Muestra una alerta

                                    Toast alerta =
                                            Toast.makeText(getContext(),
                                                    "Agregado recinto: ''"+dispositivo_nombre+"''", Toast.LENGTH_SHORT);

                                    alerta.show();

                                    //dialogFragmentsListener.actualizarListaElementos();
                                    setOnClickListener.actualizarListaRecintos();

                                }else{
                                    Log.d("CJTP","Campos incompletos.");

                                    Toast alerta =
                                            Toast.makeText(getContext(),
                                                    "Introduzca información solicitada", Toast.LENGTH_SHORT);

                                    alerta.show();
                                }


                                Log.d(TAG,"Click en agregar/guardar");

                                break;
                            default:
                                break;
                        }

                    }
                };


        CompoundButton.OnCheckedChangeListener checkedChangeListener =
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            aBoolean_switch_aux = true;
                        }else{
                            aBoolean_switch_aux = false;
                        }
                    }
                };

        //Sets the "on click listeners" to the corresponding listener, which is the locally one created.
        button_dispositivo_agregar.setOnClickListener(thisFragmentViewOnClickListener);
        aSwitch_dispositivo_estado.setOnCheckedChangeListener(checkedChangeListener);

        return thisFragmentView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Log.d(TAG,tag+"onAttach()"+tag);

        //Indica al/los listener(s) que se está(n) escuchando en el contexto de la actividad MainActivity y (Otra actividad) respectivamente.
        setOnClickListener = (OnMainActivityFragmentsListener) activity; //acciones al activity (esto está bien)

    }

}
